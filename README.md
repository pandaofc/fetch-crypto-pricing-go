# Fetch Crypto Pricing

A simple API to get Crypto pricing using Golang.
This project will sync with the latest crypto pricing `BTCUSDT` from [Binanace](https://www.binance.com/en).
When init the project, it will fetch the historical data from `2017-08-17` to `now` and store it in the `data` folder.
Then it will fetch the latest price every 1 minute and store it in the db.

## Requirement
- Docker


## Main Plugins
- [go-chi](https://github.com/go-chi/chi)
- [golang-migrate](https://github.com/golang-migrate/migrate)
- [gorm](https://pkg.go.dev/gorm.io/gorm@v1.24.2)
- [go-binance](https://pkg.go.dev/github.com/adshao/go-binance/v2@v2.3.10)
- [testify](https://pkg.go.dev/github.com/stretchr/testify@v1.8.1)
- [cron](https://pkg.go.dev/github.com/robfig/cron/v3@v3.0.1)
## Install

Please following the Docker, Docker compose Docs to install docker and docker compose:
- [Docker](https://docs.docker.com/get-docker/)
- [Docker Compose](https://docs.docker.com/compose/install/)

1. Copy and update the environment file

    ```
    cp .env.exmaple .env
    ```

2. Build the Images

    ```
    docker-compose build
    ```

## Development
```
docker-compose -f docker-compose.local.yml up
```

## Run

```
docker-compose up -d
```

## Test

```
go test ./...
```

## Endpoints

- The DEMO URL: https://demo-api.anthonyleung.xyz

- Get latest price
GET `{BASE_URL}/price`
will return latest price

- GET price by timestamp
GET `{BASE_URL}/price?timestamp={TIME}`
    - TIME is using RFC3339 Format, and only suport after 2017-8-17 04:00:00

- GET avg price
GET `{BASE_URL}/avg-price?start={START_TIME}&end={END_TIME}`
    - START_TIME is using RFC3339 Format, and only suport after 2017-8-17 04:00:00
    - END_TIME is using RFC3339 Format, and only suport after START_TIME
