package main

import (
	"fetch-crypto-pricing/cron"
	"fetch-crypto-pricing/db"
)

type App struct {
	DB   *db.DataBase
	Cron *cron.Cron
}

func (app *App) Run() {
	app.DB.Run()
	app.Cron.Run()
}

func NewApp() *App {
	return &App{
		DB:   db.NewDB(),
		Cron: cron.NewCron(),
	}
}
