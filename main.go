package main

import (
	"log"
	"net/http"
	"os"
	"time"

	"fetch-crypto-pricing/services"
	"fetch-crypto-pricing/utils"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/render"
	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	port := os.Getenv("SERVER_PORT")
	a := NewApp()
	a.Run()
	log.Println("Starting server on port: " + port)

	http.ListenAndServe(":"+port, NewRouter())
}

func NewRouter() http.Handler {
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Get("/", func(w http.ResponseWriter, _ *http.Request) {
		w.Write([]byte("welcome"))
	})
	r.Get("/price", func(w http.ResponseWriter, r *http.Request) {
		getPriceByTimestamp(w, r)
	})
	r.Get("/avg-price", func(w http.ResponseWriter, r *http.Request) {
		getAvgPriceByTimeRange(w, r)
	})
	return r
}

type LatestPriceResponse struct {
	Price float64
	Time  time.Time
}

type ErrorResponse struct {
	Error string
}

type AvgPriceResponse struct {
	AvgPrice float64
	Start    time.Time
	End      time.Time
}

func getPriceByTimestamp(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	queryTime := r.URL.Query().Get("timestamp")
	timestamp, e := utils.GetTimestamp(queryTime)
	if timestamp != nil && timestamp.Before(services.InitialTime) {
		log.Println("Timestamp is before initial time")
		w.WriteHeader(400)
		render.JSON(w, r, &ErrorResponse{Error: "Timestamp is before initial time: " + services.InitialTime.Format(time.RFC3339)})
		return
	}

	if e != nil {
		log.Println(e)
		w.WriteHeader(400)
		render.JSON(w, r, &ErrorResponse{Error: "Invalid timestamp"})
		return
	}

	log.Println(timestamp)
	fetching := services.NewFetchPriceService()
	priceRecord, _ := fetching.FetchPriceByTime(timestamp)
	render.JSON(w, r, LatestPriceResponse{Price: priceRecord.Close, Time: priceRecord.OpenTime})
}

func getAvgPriceByTimeRange(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	startTime := r.URL.Query().Get("start")
	endTime := r.URL.Query().Get("end")
	start, startError := utils.GetTimestamp(startTime)
	end, endError := utils.GetTimestamp(endTime)

	if startTime == "" || endTime == "" {
		w.WriteHeader(400)
		render.JSON(w, r, &ErrorResponse{Error: "Start and end time are required"})
		return
	}

	if startError != nil || endError != nil {
		log.Println("Invalid timestamp")
		w.WriteHeader(400)
		render.JSON(w, r, &ErrorResponse{Error: "Invalid timestamp"})
		return
	}
	if start != nil && start.Before(services.InitialTime) {
		log.Println("Start timestamp is before initial time")
		w.WriteHeader(400)
		render.JSON(w, r, &ErrorResponse{Error: "Start timestamp is before initial time: " + services.InitialTime.Format(time.RFC3339)})
		return
	}
	if start != nil && end != nil && start.After(*end) {
		log.Println("Start timestamp is after end timestamp")
		w.WriteHeader(400)
		render.JSON(w, r, &ErrorResponse{Error: "Start timestamp is after end timestamp"})
		return
	}

	fetching := services.NewFetchPriceService()
	avgPrice := fetching.FetchAvgPriceByTimeRange(*start, *end)
	render.JSON(w, r, AvgPriceResponse{AvgPrice: avgPrice, Start: *start, End: *end})

}
