package cron

import (
	"log"
	"time"

	"fetch-crypto-pricing/services"

	"github.com/go-co-op/gocron"
)

type Cron struct {
	scheduler *gocron.Scheduler
	fetching  services.FetchPriceInterface
}
type CronInterface interface {
	Run()
}

func (c *Cron) Run() {

	c.fetching.FetchHistoryPrice()
	log.Println("Cron started")

	c.scheduler.Every(1).Minute().Do(func() {
		syncTime := time.Now().Add(-1 * time.Minute)
		log.Println("Running cron... Get price at", syncTime)
		c.fetching.FetchPriceByTime(&syncTime)
	})
	c.scheduler.StartAsync()
}

func NewCron() *Cron {
	return &Cron{
		scheduler: gocron.NewScheduler(time.UTC),
		fetching:  services.NewFetchPriceService(),
	}
}
