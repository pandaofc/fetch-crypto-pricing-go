package clients

import (
	"context"
	"os"

	"github.com/adshao/go-binance/v2"
)

type Binance struct {
	client *binance.Client
}

type BinanceInterface interface {
	GetKlines(symbol string, interval string, startTime *int64, endTime *int64, limit int) ([]*binance.Kline, error)
}

func (b *Binance) GetKlines(symbol string, interval string, startTime *int64, endTime *int64, limit int) ([]*binance.Kline, error) {

	service := b.client.NewKlinesService()
	if startTime != nil {
		service.StartTime(*startTime)
	}
	if endTime != nil {
		service.EndTime(*endTime)
	}

	klines, err := service.Symbol(symbol).Interval(interval).Limit(limit).Do(context.Background())

	if err != nil {
		return nil, err
	}
	return klines, nil
}

func NewBinance() *Binance {
	apiKey := os.Getenv("BINANCE_API_KEY")
	secretKey := os.Getenv("BINANCE_SECRET_KEY")
	return &Binance{
		client: binance.NewClient(apiKey, secretKey),
	}
}
