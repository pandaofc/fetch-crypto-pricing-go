package clients

import (
	"github.com/adshao/go-binance/v2"
	"github.com/stretchr/testify/mock"
)

type BinanceMock struct {
	mock.Mock
}

func (m *BinanceMock) GetKlines(symbol string, interval string, startTime *int64, endTime *int64, limit int) ([]*binance.Kline, error) {
	args := m.Called(symbol, interval, startTime, endTime, limit)
	return args.Get(0).([]*binance.Kline), args.Error(1)
}
