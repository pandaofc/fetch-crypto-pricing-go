package repositories

import (
	"fetch-crypto-pricing/models"

	"github.com/stretchr/testify/mock"
)

type ConfigRepositoryMock struct {
	mock.Mock
}

func (m *ConfigRepositoryMock) GetConfigValue(key string) string {
	args := m.Called(key)
	return args.Get(0).(string)
}

func (m *ConfigRepositoryMock) CreateOrUpdateConfig(key string, value string) models.Config {
	args := m.Called(key, value)
	return args.Get(0).(models.Config)
}
