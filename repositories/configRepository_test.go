package repositories

import (
	"fetch-crypto-pricing/db"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func TestConfigRepository_GetConfigValue(t *testing.T) {

	mdb, mock, _ := sqlmock.New() // mock sql.DB
	gdb, _ := gorm.Open(postgres.New(postgres.Config{Conn: mdb}), &gorm.Config{})

	mockDB := &db.DataBase{DB: gdb}

	configRepo := &ConfigRepository{
		db: mockDB,
	}

	mock.ExpectQuery("SELECT \"value\" FROM \"configs\" WHERE key = \\$1").WithArgs("test").WillReturnRows(sqlmock.NewRows([]string{"key", "value"}))
	value := configRepo.GetConfigValue("test")
	if value != "" {
		t.Errorf("Expected empty string, got %s", value)
	}
}

func TestConfigRepository_CreateOrUpdateConfig(t *testing.T) {
	mdb, mock, _ := sqlmock.New() // mock sql.DB
	gdb, _ := gorm.Open(postgres.New(postgres.Config{Conn: mdb}), &gorm.Config{})

	mockDB := &db.DataBase{DB: gdb}

	configRepo := &ConfigRepository{
		db: mockDB,
	}

	mock.ExpectExec("INSERT INTO \"configs\"").WithArgs("test", "test").WillReturnResult(sqlmock.NewResult(1, 1))

	config := configRepo.CreateOrUpdateConfig("test", "test")
	if config.Key != "test" {
		t.Errorf("Expected key to be test, got %s", config.Key)
	}
	if config.Value != "test" {
		t.Errorf("Expected value to be test, got %s", config.Value)
	}

}
