package repositories

import (
	"fetch-crypto-pricing/db"
	"fetch-crypto-pricing/models"

	"gorm.io/gorm/clause"
)

type ConfigRepository struct {
	db db.DataBaseInterface
}

type ConfigRepositoryInterface interface {
	GetConfigValue(key string) string
	CreateOrUpdateConfig(key string, value string) models.Config
}

func (repo *ConfigRepository) GetConfigValue(key string) string {
	value := ""
	repo.db.GetDB().Model(&models.Config{}).Where("key = ?", key).Select("value").Scan(&value)
	return value
}
func (repo *ConfigRepository) CreateOrUpdateConfig(key string, value string) models.Config {
	config := models.Config{Key: key, Value: value}
	repo.db.GetDB().Clauses(clause.OnConflict{
		Columns:   []clause.Column{{Name: "key"}},
		DoUpdates: clause.AssignmentColumns([]string{"value"}), // column needed to be updated
	}).Create(&config)
	return config
}

func NewConfigRepository() *ConfigRepository {
	return &ConfigRepository{
		db: db.NewDB(),
	}
}
