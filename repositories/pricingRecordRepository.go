package repositories

import (
	"fetch-crypto-pricing/db"
	"fetch-crypto-pricing/models"
	"time"

	"gorm.io/gorm/clause"
)

type PricingRecordRepository struct {
	db db.DataBaseInterface
}

type PricingRecordRepositoryInterface interface {
	Create(pricingRecord models.PricingRecord) models.PricingRecord
	CreateOrUpdate(pricingRecord models.PricingRecord) models.PricingRecord
	CreateOrUpdateInBatches(pricingRecords []models.PricingRecord) ([]models.PricingRecord, error)
	FindOneByOpenTime(symbol string, interval string, openTime time.Time) *models.PricingRecord
	GetAvgPriceByTimeRange(symbol string, interval string, start time.Time, end time.Time) float64
}

func (repo *PricingRecordRepository) Create(pricingRecord models.PricingRecord) models.PricingRecord {
	repo.db.GetDB().Create(&pricingRecord)
	return pricingRecord
}

func (repo *PricingRecordRepository) FindOneByOpenTime(symbol string, interval string, openTime time.Time) *models.PricingRecord {
	pricingRecord := models.PricingRecord{}
	result := repo.db.GetDB().Where("symbol = ? AND interval = ? AND open_time = ?", symbol, interval, openTime).First(&pricingRecord)
	if result.RowsAffected == 0 {
		return nil
	}
	return &pricingRecord
}

func (repo *PricingRecordRepository) GetAvgPriceByTimeRange(symbol string, interval string, start time.Time, end time.Time) float64 {
	var avgPrice float64
	result := repo.db.GetDB().Table("pricing_records").Where("symbol = ? AND interval = ? AND open_time >= ? AND open_time <= ?", symbol, interval, start.Truncate(time.Minute), end.Truncate(time.Minute)).Select("AVG(close) as avg_price").Scan(&avgPrice)
	if result.RowsAffected == 0 {
		return 0
	}
	return avgPrice
}

func (repo *PricingRecordRepository) CreateOrUpdate(pricingRecord models.PricingRecord) models.PricingRecord {
	repo.db.GetDB().Clauses(clause.OnConflict{
		Columns:   []clause.Column{{Name: "open_time"}, {Name: "symbol"}, {Name: "interval"}},
		DoUpdates: clause.AssignmentColumns([]string{"open", "high", "low", "close", "volume", "close_time"}),
	}).Create(&pricingRecord)

	return pricingRecord
}

func (repo *PricingRecordRepository) CreateOrUpdateInBatches(pricingRecords []models.PricingRecord) ([]models.PricingRecord, error) {
	result := repo.db.GetDB().Clauses(clause.OnConflict{
		Columns:   []clause.Column{{Name: "open_time"}, {Name: "symbol"}, {Name: "interval"}},
		DoUpdates: clause.AssignmentColumns([]string{"open", "high", "low", "close", "volume", "close_time"}),
	}).CreateInBatches(pricingRecords, 1000)

	return pricingRecords, result.Error
}

func NewPricingRecordRepository() PricingRecordRepositoryInterface {
	return &PricingRecordRepository{
		db: db.NewDB(),
	}
}
