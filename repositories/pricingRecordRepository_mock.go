package repositories

import (
	"fetch-crypto-pricing/models"
	"time"

	"github.com/stretchr/testify/mock"
)

type PricingRecordRepositoryMock struct {
	mock.Mock
}

func (m *PricingRecordRepositoryMock) Create(pricingRecord models.PricingRecord) models.PricingRecord {
	args := m.Called(pricingRecord)
	return args.Get(0).(models.PricingRecord)
}

func (m *PricingRecordRepositoryMock) CreateOrUpdate(pricingRecord models.PricingRecord) models.PricingRecord {
	args := m.Called(pricingRecord)
	return args.Get(0).(models.PricingRecord)
}

func (m *PricingRecordRepositoryMock) CreateOrUpdateInBatches(pricingRecords []models.PricingRecord) ([]models.PricingRecord, error) {
	args := m.Called(pricingRecords)
	return args.Get(0).([]models.PricingRecord), args.Error(1)
}

func (m *PricingRecordRepositoryMock) FindOneByOpenTime(symbol string, interval string, openTime time.Time) *models.PricingRecord {
	args := m.Called(symbol, interval, openTime)
	return args.Get(0).(*models.PricingRecord)
}

func (m *PricingRecordRepositoryMock) GetAvgPriceByTimeRange(symbol string, interval string, start time.Time, end time.Time) float64 {
	args := m.Called(symbol, interval, start, end)
	return args.Get(0).(float64)
}
