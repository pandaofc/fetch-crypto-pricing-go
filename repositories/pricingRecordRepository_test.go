package repositories

import (
	"fetch-crypto-pricing/db"
	"fetch-crypto-pricing/models"
	"regexp"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func TestPricingRecordRepository_Create(t *testing.T) {

	mdb, mock, _ := sqlmock.New() // mock sql.DB
	gdb, _ := gorm.Open(postgres.New(postgres.Config{Conn: mdb}), &gorm.Config{})

	mockDB := &db.DataBase{DB: gdb}

	repo := &PricingRecordRepository{
		db: mockDB,
	}
	p := models.PricingRecord{
		Symbol:    "test",
		Interval:  "test",
		Volume:    1,
		Open:      1,
		High:      1,
		Low:       1,
		Close:     1,
		OpenTime:  time.Now(),
		CloseTime: time.Now(),
	}
	mock.ExpectExec("INSERT INTO \"pricing_records\"").WithArgs("test", "test", 1.0, 1.0, 1.0, 1.0, 1.0, time.Now(), time.Now()).WillReturnResult(sqlmock.NewResult(1, 1))

	pricingRecord := repo.Create(p)
	if pricingRecord.Symbol != "test" {
		t.Errorf("Expected symbol to be test, got %s", pricingRecord.Symbol)
	}
	if pricingRecord.Interval != "test" {
		t.Errorf("Expected interval to be test, got %s", pricingRecord.Interval)
	}
	if pricingRecord.Volume != 1 {
		t.Errorf("Expected volume to be 1, got %f", pricingRecord.Volume)
	}
}

func TestPricingRecordRepository_FindOneByOpenTime(t *testing.T) {

	mdb, mock, _ := sqlmock.New() // mock sql.DB
	gdb, _ := gorm.Open(postgres.New(postgres.Config{Conn: mdb}), &gorm.Config{})

	mockDB := &db.DataBase{DB: gdb}

	repo := &PricingRecordRepository{
		db: mockDB,
	}
	timeNow := time.Date(2017, 8, 17, 4, 0, 0, 0, time.UTC)

	mock.ExpectQuery(regexp.QuoteMeta(`SELECT * FROM "pricing_records" WHERE symbol = $1 AND interval = $2 AND open_time = $3 ORDER BY "pricing_records"."created_at" LIMIT 1`)).WithArgs("test", "test", timeNow).WillReturnRows(sqlmock.NewRows([]string{"id", "symbol", "interval", "volume", "open", "high", "low", "close", "open_time", "close_time", "created_at", "updated_at"}).AddRow(1, "test", "test", 1.0, 1.0, 1.0, 1.0, 1.0, timeNow, timeNow, timeNow, timeNow))
	pricingRecord := repo.FindOneByOpenTime("test", "test", timeNow)
	if pricingRecord.Symbol != "test" {
		t.Errorf("Expected symbol to be test, got %s", pricingRecord.Symbol)
	}
	if pricingRecord.Interval != "test" {
		t.Errorf("Expected interval to be test, got %s", pricingRecord.Interval)
	}
	if pricingRecord.Volume != 1 {
		t.Errorf("Expected volume to be 1, got %f", pricingRecord.Volume)
	}
}

func TestPriceRepository_GetAvgPriceByTimeRange(t *testing.T) {

	mdb, mock, _ := sqlmock.New() // mock sql.DB
	gdb, _ := gorm.Open(postgres.New(postgres.Config{Conn: mdb}), &gorm.Config{})

	mockDB := &db.DataBase{DB: gdb}

	repo := &PricingRecordRepository{
		db: mockDB,
	}
	timeNow := time.Date(2017, 8, 17, 4, 0, 0, 0, time.UTC)

	mock.ExpectQuery(regexp.QuoteMeta(`SELECT AVG(close) as avg_price FROM "pricing_records" WHERE symbol = $1 AND interval = $2 AND open_time >= $3 AND open_time <= $4`)).WithArgs("test", "test", timeNow, timeNow).WillReturnRows(sqlmock.NewRows([]string{"avg"}).AddRow(1.0))
	avgPrice := repo.GetAvgPriceByTimeRange("test", "test", timeNow, timeNow)
	if avgPrice != 1.0 {
		t.Errorf("Expected avg price to be 1.0, got %f", avgPrice)
	}
}
