package services

import (
	"log"
	"os"
	"sync"
	"time"

	"fetch-crypto-pricing/clients"
	"fetch-crypto-pricing/models"
	"fetch-crypto-pricing/repositories"
	"fetch-crypto-pricing/utils"

	"github.com/adshao/go-binance/v2"
)

type FetchPriceService struct {
	binance           clients.BinanceInterface
	pricingRecordRepo repositories.PricingRecordRepositoryInterface
	configRepo        repositories.ConfigRepositoryInterface
}

type FetchPriceInterface interface {
	FetchPriceByTime(*time.Time) (models.PricingRecord, error)
	FetchAvgPriceByTimeRange(time.Time, time.Time) float64
	FetchHistoryPrice()
}

var symbol = "BTCUSDT"
var interval = "1m"
var perTotal = 1000
var preTh = 10

// Binance inital time
var InitialTime time.Time = time.Date(2017, 8, 17, 4, 0, 0, 0, time.UTC)

func (f *FetchPriceService) FetchPriceByTime(selectedTime *time.Time) (models.PricingRecord, error) {
	log.Println("Fetching price...")
	var t *int64 = nil
	var tb *int64 = nil
	if selectedTime == nil || selectedTime.Truncate(time.Minute).Equal(time.Now().Truncate(time.Minute)) {
		log.Println("Fetching current price")
		return f.fetchCurrentPrice()
	}

	// If getting latest price, the pricing may not be ready, so we not save it to DB
	if selectedTime == nil || selectedTime.Truncate(time.Minute).Equal(time.Now().Truncate(time.Minute)) {
		klines, err := f.binance.GetKlines(symbol, interval, t, tb, 1)
		if err != nil {
			log.Println(err)
			return models.PricingRecord{}, err
		}
		return utils.MapKlineToPricingRecord(klines[0], symbol, interval), nil
	}

	log.Println("Fetching price by time...")
	sameRec := f.pricingRecordRepo.FindOneByOpenTime(symbol, interval, selectedTime.Truncate(time.Minute))
	if sameRec != nil {
		log.Println("Found same record in DB")
		return *sameRec, nil
	}
	roundedTime := selectedTime.Truncate(time.Minute).UnixMilli()
	t = &roundedTime

	klines, err := f.binance.GetKlines(symbol, interval, t, tb, 1)

	if err != nil {
		log.Println(err)
		return models.PricingRecord{}, err
	}
	p := f.insertPricingRecordByKline(klines[len(klines)-1])
	f.updateSyncTime(p.OpenTime)
	return p, nil
}

func (f *FetchPriceService) fetchCurrentPrice() (models.PricingRecord, error) {

	klines, err := f.binance.GetKlines(symbol, interval, nil, nil, 1)
	if err != nil {
		log.Println(err)
		return models.PricingRecord{}, err
	}
	return utils.MapKlineToPricingRecord(klines[0], symbol, interval), nil

}

func (f *FetchPriceService) FetchAvgPriceByTimeRange(start time.Time, end time.Time) float64 {
	return f.pricingRecordRepo.GetAvgPriceByTimeRange(symbol, interval, start, end)
}

func (f *FetchPriceService) FetchHistoryPrice() {
	log.Println("Fetching history price...")
	// Get DB last sync time
	var lastSyncTime time.Time = InitialTime
	value := f.configRepo.GetConfigValue("last_sync_time")
	if value == "" {
		log.Println("No history price in DB, using default time InitialTime = ", InitialTime)
	} else {
		lastSyncTime, _ = time.Parse(time.RFC3339, value)
		log.Println("Last sync time = ", lastSyncTime)
	}
	var processingTime time.Time = lastSyncTime.Add(time.Minute)
	if (time.Now().Unix() - processingTime.Unix()) < 60 {
		log.Println("No need to fetch history price")
		return
	}
	for processingTime.Before(time.Now()) {
		var tmpKlines = make([]*binance.Kline, 0)
		var times []time.Time
		var tmpPricingRecords = make([]models.PricingRecord, 0)
		for i := 0; i < preTh; i++ {
			times = append(times, processingTime)
			processingTime = processingTime.Add(time.Duration(perTotal) * time.Minute)
		}
		logger := log.New(os.Stdout, "", 0)
		logger.Println("Fetching price from ", times[0], " to ", times[len(times)-1])
		logger.Println("tmpPricingRecords: ", len(tmpPricingRecords))
		var wg sync.WaitGroup
		for _, t := range times {
			wg.Add(1)
			go func() {
				logger.Println("Fetching history price from: ", t)
				unixTime := t.UnixMilli()
				klines, err := f.binance.GetKlines(symbol, interval, &unixTime, nil, perTotal)
				if err != nil {
					log.Println(err)
					return
				}
				// Insert records to DB
				tmpKlines = append(tmpKlines, klines...)
				defer wg.Done()
				logger.Println(len(tmpKlines))
			}()
			// Wait all goroutines.
			wg.Wait()
		}
		for _, kline := range tmpKlines {
			tmpPricingRecords = append(tmpPricingRecords, utils.MapKlineToPricingRecord(kline, symbol, interval))
		}
		log.Println(tmpPricingRecords[0].OpenTime, tmpPricingRecords[len(tmpPricingRecords)-1].OpenTime)
		f.insertBatchPricingRecord(tmpPricingRecords)
		f.updateSyncTime(tmpPricingRecords[len(tmpPricingRecords)-1].OpenTime)
	}
}

func (f *FetchPriceService) insertPricingRecordByKline(kline *binance.Kline) models.PricingRecord {
	pricingRecord := utils.MapKlineToPricingRecord(kline, symbol, interval)
	return f.pricingRecordRepo.CreateOrUpdate(pricingRecord)
}

func (f *FetchPriceService) insertBatchPricingRecord(pricingRecords []models.PricingRecord) ([]models.PricingRecord, error) {
	p, err := f.pricingRecordRepo.CreateOrUpdateInBatches(pricingRecords)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	return p, nil
}

func (f *FetchPriceService) updateSyncTime(lastSyncTime time.Time) {
	f.configRepo.CreateOrUpdateConfig("last_sync_time", lastSyncTime.Format(time.RFC3339))
}

func NewFetchPriceService() *FetchPriceService {
	return &FetchPriceService{
		binance:           clients.NewBinance(),
		pricingRecordRepo: repositories.NewPricingRecordRepository(),
		configRepo:        repositories.NewConfigRepository(),
	}
}
