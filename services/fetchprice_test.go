package services

import (
	"fetch-crypto-pricing/clients"
	"fetch-crypto-pricing/models"
	"fetch-crypto-pricing/repositories"
	"testing"
	"time"

	"github.com/adshao/go-binance/v2"
)

func TestFetchPriceService_FetchPriceByTime(t *testing.T) {
	prRepoMock := &repositories.PricingRecordRepositoryMock{}
	configRepoMock := &repositories.ConfigRepositoryMock{}
	binanceMock := &clients.BinanceMock{}

	timeNow := time.Now()
	nowUnix := timeNow.UnixMilli()

	selectedTime := timeNow.Add(-time.Minute * 5)
	selectedTimeUnix := selectedTime.UnixMilli()

	kline := binance.Kline{
		Open:      "1",
		High:      "3",
		Low:       "4",
		Close:     "5",
		OpenTime:  nowUnix,
		CloseTime: nowUnix,
	}
	kline2 := binance.Kline{
		Open:      "1",
		High:      "3",
		Low:       "4",
		Close:     "5",
		OpenTime:  selectedTimeUnix,
		CloseTime: selectedTimeUnix,
	}
	var ta *int64 = nil
	var tb *int64 = nil
	binanceMock.On("GetKlines", "BTCUSDT", "1m", ta, tb, 1).Return([]*binance.Kline{&kline}, nil)
	binanceMock.On("GetKlines", "BTCUSDT", "1m", selectedTimeUnix, tb, 1).Return([]*binance.Kline{&kline2}, nil)

	expectPricingRecord := models.PricingRecord{
		Symbol:    "BTCUSDT",
		Interval:  "1m",
		Open:      1,
		High:      3,
		Low:       4,
		Close:     5,
		OpenTime:  selectedTime,
		CloseTime: selectedTime,
	}
	prRepoMock.On("FindOneByOpenTime", "BTCUSDT", "1m", selectedTime.Truncate(time.Minute)).Return(&expectPricingRecord, nil)

	fetchPriceService := &FetchPriceService{
		binance:           binanceMock,
		pricingRecordRepo: prRepoMock,
		configRepo:        configRepoMock,
	}

	pricingRecord, err := fetchPriceService.FetchPriceByTime(nil)
	if err != nil {
		t.Errorf("expected no error, but %s", err)
	}
	if pricingRecord.OpenTime.Truncate(time.Minute) != timeNow.Truncate(time.Minute) {
		t.Errorf("expected open time to be %s, but %s", timeNow.Truncate(time.Minute), pricingRecord.OpenTime.Truncate(time.Minute))
	}
	if pricingRecord.CloseTime.Truncate(time.Minute) != timeNow.Truncate(time.Minute) {
		t.Errorf("expected close time to be %s, but %s", timeNow.Truncate(time.Minute), pricingRecord.OpenTime.Truncate(time.Minute))
	}

	pricingRecord2, err2 := fetchPriceService.FetchPriceByTime(&selectedTime)
	if err2 != nil {
		t.Errorf("expected no error, but %s", err2)
	}
	if pricingRecord2.OpenTime.Truncate(time.Minute) != selectedTime.Truncate(time.Minute) {
		t.Errorf("expected open time to be %s, but %s", selectedTime.Truncate(time.Minute), pricingRecord.OpenTime.Truncate(time.Minute))
	}
	if pricingRecord2.CloseTime.Truncate(time.Minute) != selectedTime.Truncate(time.Minute) {
		t.Errorf("expected close time to be %s, but %s", selectedTime.Truncate(time.Minute), pricingRecord.OpenTime.Truncate(time.Minute))
	}
}

func TestFetchPrice_FetchAvgPriceByTimeRange(t *testing.T) {
	prRepoMock := &repositories.PricingRecordRepositoryMock{}
	configRepoMock := &repositories.ConfigRepositoryMock{}
	binanceMock := &clients.BinanceMock{}
	start := time.Now().Add(-time.Minute * 5)
	end := time.Now()
	prRepoMock.On("GetAvgPriceByTimeRange", "BTCUSDT", "1m", start, end).Return(1.0)

	fetchPriceService := &FetchPriceService{
		binance:           binanceMock,
		pricingRecordRepo: prRepoMock,
		configRepo:        configRepoMock,
	}

	avgPrice := fetchPriceService.FetchAvgPriceByTimeRange(start, end)
	if avgPrice != 1.0 {
		t.Errorf("expected avg price to be 1.0, but %f", avgPrice)
	}
}
