package models

import (
	"time"
)

type PricingRecord struct {
	BaseModel
	Symbol    string    `gorm:"type:varchar(30);"`
	Interval  string    `gorm:"type:varchar(30);"`
	Volume    float64   `gorm:"type:numeric;"`
	Open      float64   `gorm:"type:numeric;"`
	High      float64   `gorm:"type:numeric;"`
	Low       float64   `gorm:"type:numeric;"`
	Close     float64   `gorm:"type:numeric;"`
	OpenTime  time.Time `gorm:"type:timestamp;"`
	CloseTime time.Time `gorm:"type:timestamp;"`
}
