package models

import (
	"time"

	"gorm.io/gorm"
)

type BaseModel struct {
	// ID        uuid.UUID
	CreatedAt time.Time
	UpdatedAt time.Time
}

func (model *BaseModel) BeforeCreate(db *gorm.DB) (err error) {
	model.CreatedAt = time.Now()
	model.UpdatedAt = time.Now()
	return
}

func (model *BaseModel) BeforeUpdate(db *gorm.DB) (err error) {
	model.UpdatedAt = time.Now()
	return
}
