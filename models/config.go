package models

type Config struct {
	BaseModel
	Key   string `gorm:"type:varchar(30);"`
	Value string `gorm:"type:varchar(255);"`
}
