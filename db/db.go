//go:build !testing
// +build !testing

package db

import (
	"fmt"
	"log"
	"os"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

type DataBaseInterface interface {
	GetDB() *gorm.DB
}

type DataBase struct {
	DB *gorm.DB
}

var dsn = fmt.Sprintf(
	"host=%s user=%s password=%s dbname=%s port=%s sslmode=disable",
	os.Getenv("POSTGRES_HOST"),
	os.Getenv("POSTGRES_USER"),
	os.Getenv("POSTGRES_PASSWORD"),
	os.Getenv("POSTGRES_DB"),
	os.Getenv("POSTGRES_PORT"),
)

var pgcon = postgres.New(postgres.Config{
	DSN:                  dsn,
	PreferSimpleProtocol: true,
})

func connectDB() *gorm.DB {
	db, err := gorm.Open(pgcon, &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			SingularTable: false,
		},
	})
	if err != nil {
		log.Println("Connect DB failed: ", err)
		panic(err)
	}

	log.Println("DB Connected.")
	return db.Session(&gorm.Session{PrepareStmt: true})
}

func (d *DataBase) GetDB() *gorm.DB {
	return d.DB
}

func migration() {
	migration := NewMigration()
	migration.Up()
}

func NewDB() *DataBase {
	db := connectDB()
	return &DataBase{DB: db}
}

func (d *DataBase) Run() {
	migration()
	log.Println("Init DB")
}
