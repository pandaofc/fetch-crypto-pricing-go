package db

import (
	"fmt"
	"log"
	"os"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
)

type Migration struct {
	client *migrate.Migrate
}

var dbUrl = fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable",
	os.Getenv("POSTGRES_USER"),
	os.Getenv("POSTGRES_PASSWORD"),
	os.Getenv("POSTGRES_HOST"),
	os.Getenv("POSTGRES_PORT"),
	os.Getenv("POSTGRES_DB"),
)

func NewMigration() *Migration {
	m := Migration{}
	path := "file://_assets/db/migration"
	var err error
	m.client, err = migrate.New(path, dbUrl)
	if err != nil {
		panic(err)
	}
	return &m
}

func (m *Migration) To(targetVersion uint) {
	if err := m.client.Migrate(targetVersion); err != nil && err != migrate.ErrNoChange {
		panic(err)
	}
	afterVersion, _, _ := m.client.Version()
	log.Printf("Migration to version:%d success", afterVersion)
}

func (m *Migration) Up() {
	if err := m.client.Up(); err != nil && err != migrate.ErrNoChange {
		panic(err)
	}
	afterVersion, _, _ := m.client.Version()
	log.Printf("Migration up version:%d success", afterVersion)
}

func (m *Migration) Down() {
	if err := m.client.Down(); err != nil && err != migrate.ErrNoChange {
		panic(err)
	}
	version, _, _ := m.client.Version()
	log.Printf("Migration down version:%d success", version)
}
