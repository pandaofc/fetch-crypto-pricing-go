package utils

import (
	"testing"

	"github.com/adshao/go-binance/v2"
)

func TestGetTimestamp(t *testing.T) {
	timestamp, err := GetTimestamp("2021-11-22T17:30:50Z")
	if err != nil {
		t.Errorf("expected no error, but %s", err)
	}
	if timestamp.IsZero() {
		t.Errorf("expected timestamp to be set, but %s", timestamp)
	}

	timestamp, err = GetTimestamp("2021-11-22T17:30")
	if err == nil {
		t.Errorf("expected error, but %s", err)
	}

	if timestamp != nil {
		t.Errorf("expected timestamp to be nil, but %s", timestamp)
	}
}

func TestMapKlineToPricingRecord(t *testing.T) {
	kline := &binance.Kline{
		Open:      "1",
		Volume:    "2",
		High:      "3",
		Low:       "4",
		Close:     "5",
		OpenTime:  1637673050000,
		CloseTime: 1637673050000,
	}
	pricingRecord := MapKlineToPricingRecord(kline, "BTCUSDT", "1m")
	if pricingRecord.Symbol != "BTCUSDT" {
		t.Errorf("expected symbol to be BTCUSDT, but %s", pricingRecord.Symbol)
	}
	if pricingRecord.Interval != "1m" {
		t.Errorf("expected interval to be 1m, but %s", pricingRecord.Interval)
	}
	if pricingRecord.Volume != 2 {
		t.Errorf("expected volume to be 2, but %f", pricingRecord.Volume)
	}
	if pricingRecord.Open != 1 {
		t.Errorf("expected open to be 1, but %f", pricingRecord.Open)
	}
	if pricingRecord.High != 3 {
		t.Errorf("expected high to be 3, but %f", pricingRecord.High)
	}
	if pricingRecord.Low != 4 {
		t.Errorf("expected low to be 4, but %f", pricingRecord.Low)
	}
	if pricingRecord.Close != 5 {
		t.Errorf("expected close to be 5, but %f", pricingRecord.Close)
	}
	if pricingRecord.OpenTime.IsZero() {
		t.Errorf("expected openTime to be set, but %s", pricingRecord.OpenTime)
	}
	if pricingRecord.CloseTime.IsZero() {
		t.Errorf("expected closeTime to be set, but %s", pricingRecord.CloseTime)
	}
}
