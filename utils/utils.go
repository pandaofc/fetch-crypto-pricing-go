package utils

import (
	"fetch-crypto-pricing/models"
	"log"
	"strconv"
	"time"

	"github.com/adshao/go-binance/v2"
)

func GetTimestamp(queryTime string) (*time.Time, error) {
	if queryTime == "" {
		return nil, nil
	}
	t, err := time.Parse(time.RFC3339, queryTime)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	return &t, nil
}

func MapKlineToPricingRecord(kline *binance.Kline, symbol string, interval string) models.PricingRecord {
	openPrice, _ := strconv.ParseFloat(kline.Open, 64)
	volume, _ := strconv.ParseFloat(kline.Volume, 64)
	high, _ := strconv.ParseFloat(kline.High, 64)
	low, _ := strconv.ParseFloat(kline.Low, 64)
	close, _ := strconv.ParseFloat(kline.Close, 64)
	openTime := time.UnixMilli(kline.OpenTime)
	closeTime := time.UnixMilli(kline.CloseTime)
	pricingRecord := models.PricingRecord{
		Symbol:    symbol,
		Interval:  interval,
		Volume:    volume,
		Open:      openPrice,
		High:      high,
		Low:       low,
		Close:     close,
		OpenTime:  openTime,
		CloseTime: closeTime,
	}

	return pricingRecord
}
