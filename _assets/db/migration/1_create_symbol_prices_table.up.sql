-- 1_create_symbol_prices_table.up.sql

BEGIN;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE
    pricing_records (
        id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
        symbol VARCHAR(30),
        interval VARCHAR(30),
        volume NUMERIC(12, 6),
        open NUMERIC(12, 6),
        high NUMERIC(12, 6),
        low NUMERIC(12, 6),
        close NUMERIC(12, 6),
        open_time TIMESTAMPTZ,
        close_time TIMESTAMPTZ,
        created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
        updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
    );

CREATE INDEX symbol_idx ON pricing_records (symbol);

CREATE INDEX
    symbol_interval_idx ON pricing_records (symbol, interval);

CREATE INDEX
    symbol_interval_open_time_idx ON pricing_records (symbol, interval, open_time);

CREATE UNIQUE INDEX
    symbol_interval_open_time_unique_idx ON pricing_records (symbol, interval, open_time);

CREATE TABLE
    configs (
        id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
        key VARCHAR(30),
        value VARCHAR(255),
        created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
        updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
    );

CREATE UNIQUE INDEX key_idx ON configs (key);

COMMIT;