### Builder
FROM golang:1.19.3-alpine3.15 AS builder

WORKDIR /app

RUN apk add --no-cache ca-certificates

# Copy and download dependency using go mod.
COPY go.mod go.sum ./
RUN go mod download

COPY . .

# Set necessary environment variables needed for our image and build the API server.
ENV CGO_ENABLED=0 GOOS=linux GOARCH=amd64
RUN go build -ldflags="-s -w" -o api .

### Development with hot reload and debugger
FROM cosmtrek/air AS dev

COPY --from=builder ["/app/api", "/app/.env", "/"]

CMD ["air"]

### Production
FROM scratch AS production

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
# Copy binary and config files from /app to root folder of scratch container.
COPY --from=builder ["/app/api", "/app/.env", "/"]

# Command to run when starting the container.
ENTRYPOINT ["/api"]
